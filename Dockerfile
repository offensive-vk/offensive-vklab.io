# Use official Node.js 20 LTS image
FROM node:20

# Set working directory in the container
WORKDIR /app

# Copy package.json and pnpm-lock.yaml to leverage caching
COPY package.json pnpm-lock.yaml ./

# Install pnpm and dependencies
RUN npm install -g pnpm@9.10.0 && pnpm i --frozen-lockfile

# Copy the rest of the application files
COPY . .

# Build the project
RUN pnpm run build

# Expose port
EXPOSE 7000

# Command to run your application in production mode
CMD ["pnpm", "run", "dev"]

# Apply some copyrights
LABEL license="MIT"
LABEL author="Vedansh <superuser.ntsystems@outlook.com>"