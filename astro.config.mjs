// ** DO NOT MODIFY THIS FILE **
import { defineConfig } from 'astro/config';

export default defineConfig({
  sitemap: true,
  site: 'https://offensive-vk.gitlab.io',
  outDir: 'public',
  publicDir: 'static',
});
// ** DO NOT MODIFY THIS FILE **